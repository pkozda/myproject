from todo import views
from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'todo', views.TodoViewSet)
router.register(r'todo/(?P<todo_pk>\d+)/tasks', views.TaskViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'connections', views.ConnectionsViewSet)

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^api/', include(router.urls))
)
