from rest_framework import serializers
from todo.models import List, ListUsers, Task
from django.contrib.auth.admin import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class ListUsersSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    list = serializers.Field(source='list.name')

    class Meta:
        model = ListUsers
        fields = ('user', 'list', 'permission')


class ListSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True, read_only=True)

    class Meta:
        model = List
        fields = ('id', 'name', 'created_date', 'users', 'task_set')


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('name', 'description', 'time', 'completed')
