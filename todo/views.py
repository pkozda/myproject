from django.contrib.auth.admin import User
from rest_framework import viewsets, permissions
from todo.serializers import (UserSerializer, ListSerializer,
                              ListUsersSerializer, TaskSerializer)
from todo.models import ListUsers, List, Task
from django.shortcuts import render
from rest_framework.authentication import TokenAuthentication


def index(request):
    return render(request, 'todo/index.html')


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This endpoint presents the users in the system.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAdminUser,)


class ConnectionsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This endpoint presents the listusers in the system.
    """
    queryset = ListUsers.objects.all()
    serializer_class = ListUsersSerializer
    permission_classes = (permissions.IsAdminUser,)


class TodoViewSet(viewsets.ModelViewSet):
    """
    This endpoint presents the todo lists in the system.
    """
    queryset = List.objects.all()
    serializer_class = ListSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        queryset = super(TodoViewSet, self).get_queryset()
        print(self.kwargs)
        return queryset

    def perform_create(self, serializer):
        new_list = serializer.save()
        ListUsers.objects.create(user=self.request.user,
                                 list=new_list,
                                 permission='a')


class TaskViewSet(viewsets.ModelViewSet):
    """
    This endpoint presents tasks of current todo list.
    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        queryset = super(TaskViewSet, self).get_queryset()
        return queryset.filter(list__pk=self.kwargs.get('todo_pk'))

    def perform_create(self, serializer):
        list_pk = self.kwargs.get('todo_pk')
        new_list = List.objects.get(pk=list_pk)
        serializer.save(list=new_list)
