from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class List(models.Model):
    """Model for creating list of future tasks"""

    name = models.CharField(max_length=100)
    users = models.ManyToManyField('auth.User', through='ListUsers')
    created_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name


class ListUsers(models.Model):
    """Middleware model with permissions."""

    PERMISSIONS = (
        ('a', 'Author'),
        ('r', 'Read only'),
        ('e', 'Read and edit'),
    )

    user = models.ForeignKey('auth.User')
    list = models.ForeignKey('List')
    permission = models.CharField(max_length=1, choices=PERMISSIONS)


class Task(models.Model):
    """Model for creating task of the list"""

    name = models.CharField(max_length=100)
    list = models.ForeignKey('List')
    description = models.TextField(blank=True, null=True)
    time = models.TimeField()
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.name
